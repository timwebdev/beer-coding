window.addEventListener('load', _ => {
    const starfield = document.getElementById('starfield');
    const ctx = starfield.getContext('2d');
    let width = 600;
    let height = 600;
    const velocityX = 0.4;
    const starColor = "#ffffff";
    const gradientTop = "#0b182b";
    const gradientBottom = "#233e51";
    const minStarBrightness = 25;
    const starDensity = 0.004;
    let starCount = height * width * starDensity;
    let stars = [];
    const mountainsUrl = './images/mountain1920.jpg';
    let mountainsCanvas = null;

    const resizeHandler = _ => {
        width = document.documentElement.clientWidth;
        starfield.setAttribute('width', width);
        starfield.setAttribute('height', height);
        starCount = height * width * starDensity;
        stars = generateStars(starCount);
    };

    const generateStars = (qty) => {
        let stars = [];
        for (let i = 0; i < qty; i++) {
            let x = Math.random() * width;
            let y = Math.random() * height;
            let alpha = Math.floor(Math.random() * (256 - minStarBrightness)) + minStarBrightness;
            let color = starColor + alpha.toString(16);
            stars.push([x, y, color]);
        }
        return stars;
    };

    const background = _ => {
        var grd = ctx.createLinearGradient(0, 0, 0, height);
        grd.addColorStop(0, gradientTop);
        grd.addColorStop(1, gradientBottom);

        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, width, height);
    };

    const preprocessImage = img => {
        const c = document.createElement('canvas');
        c.width = img.width;
        c.height = img.height;

        const ctx = c.getContext('2d');

        ctx.drawImage(img, 0, 0, img.width, img.height);
        const imageData = ctx.getImageData(0, 0, img.width, img.height);
        const pixels = imageData.data;

        const r = 0, g = 1, b = 2, a = 3;
        for (let p = 0; p < pixels.length; p += 4) {
            if (pixels[p + r] +
                pixels[p + g] +
                pixels[p + b] > 400) // if white then change alpha to 0
            // since image is very dark, we can assume white is anything where r+g+b > 400
            { pixels[p + a] = 0; }
        }

        ctx.putImageData(imageData, 0, 0);
        return c;
    };
    //https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images


    const renderFrame = _ => {
        background();

        // const imageData = ctx.getImageData(0, 0, width, height);
        // const pixels = imageData.data;
        // let index = 0;
        let velocityY = 0;
        for (let i in stars) {
            stars[i][0] += velocityX;
            // set velocityY to the derivative of circle formula, shifted by -width/2
            // velocityY = (2 * stars[i][0] - width) / (5 * width);
            let x_minus_half_width = stars[i][0] - (width / 2);
            velocityY = x_minus_half_width / Math.sqrt(5 * width * width - x_minus_half_width * x_minus_half_width);
            stars[i][1] += velocityY;

            if (stars[i][0] > width) {
                stars[i][0] = 0;
                stars[i][1] = Math.random() * height;
            } else if (stars[i][1] > height && stars[i][0] > width / 2) {
                stars[i][1] = 0;
                stars[i][0] = width - Math.random() * Math.random() * width / 2; // chi-squared distribution, because the stars need to be added mostly near the edges
            } else if (stars[i][1] < 0 && stars[i][0] <= width / 2) {
                stars[i][1] = height;
                stars[i][0] = Math.random() * Math.random() * width / 2; // chi-squared distribution, because the stars need to be added mostly near the edges
            }
            ctx.fillStyle = stars[i][2];
            ctx.fillRect(stars[i][0], stars[i][1], 1, 1);

            // index = 4 * (Math.floor(stars[i][1]) * width + Math.floor(stars[i][0]));
            // // console.log(index);
            // pixels[index + 0] = 0;
            // pixels[index + 1] = 0;
            // pixels[index + 2] = 0;
            // pixels[index + 3] = 0;
        }

        // ctx.putImageData(imageData, 0, 0);


        if (mountainsCanvas !== null) {
            let aspect = mountainsCanvas.height / mountainsCanvas.width

            ctx.drawImage(mountainsCanvas, 0, height - width * aspect, width, width * aspect);
        }
        window.requestAnimationFrame(renderFrame);
    }
    var mountainsImage = new Image();
    mountainsImage.crossOrigin = 'anonymous';
    mountainsImage.src = mountainsUrl;
    mountainsImage.onload = function () {
        mountainsCanvas = preprocessImage(this)
    };

    // mountainsCanvas = preprocessImage(document.getElementById(mountainsImage));
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
    window.requestAnimationFrame(renderFrame);
});