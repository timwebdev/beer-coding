var canvas, context;

var balls = [];
var ball_count = 10;
var previous = Date.now();
// var gravity = 6.67408e-11;
var gravity = 400000;
var bounce_elasticity = 0.8;

var gravity_direction = 1; // set to -1 when repelling

// calculate dragging and mouse clicking
var dragging_ball;
var drag_start_time;
var drag_record = [];

function setup(){
    canvas = document.getElementById('the-canvas');
    context = canvas.getContext('2d');

    window.addEventListener('resize', resize, false); 
    window.addEventListener('keydown', keydown, false); 
    window.addEventListener('keyup', keyup, false); 

    canvas.addEventListener('mousedown', mousedown, false); 
    canvas.addEventListener('mouseup', mouseup, false); 
    resize();

    // place balls randomly, checking for overlaps but avoid infinite loops
    var attempts = 1000;
    for (var i = 0; i<ball_count;i++) {
        var placed = false;
        while (!placed && attempts > 0 ) {
            var newBall = new Ball();
            var position_valid = true;
            for (let ball of balls) {
                if (newBall.collision(ball)){
                    position_valid = false
                    break; // just breaks inner for
                }
            }
            if (position_valid) {
                balls.push(newBall);
                placed = true;
            }
            else attempts--;
        }
    }
    draw();
}

function mousedown(e) {
    var mousePos = new Vector(e.offsetX, e.offsetY);
    for (let ball of balls) {
        if (Vector.sub(ball.p,mousePos).getMag() < ball.r) {
            dragging_ball = ball;
            dragging_ball.isDragging = true;
            drag_start_time = Date.now()
            ball.p = mousePos;
            drag_record.push({time:Date.now(), pos:this.p}); // only capture during frames;
            canvas.removeEventListener('mousemove', mousemove); 
            canvas.addEventListener('mousemove', mousemove, false); 

            // got a ball
            break; // make sure only one ball can be dragging
        }
    }
}

function mousemove(e) {
    if (dragging_ball) {
        dragging_ball.p = new Vector(e.offsetX, e.offsetY);
    } 
}

function mouseup(e) {
    if (dragging_ball) {
        if (Date.now() - drag_start_time < 250) {
            dragging_ball.v.clear();
        }
        else {
            var mousePos = new Vector(e.offsetX, e.offsetY);

            var iStart = Math.max(0,drag_record.length - 10);
            var dt = Date.now() - drag_record[iStart].time;
            var previousPos = drag_record[iStart].pos;
            var dist = new Vector(0,0);
           
            for (let i = iStart + 1; i < drag_record.length ; i++) {
                dist.add(Vector.sub(drag_record[i].pos, previousPos));
                previousPos = drag_record[i].pos;
            }
            dist.mult(1000/dt);
            dragging_ball.v = dist;//, 1000 / dt);
        }

        dragging_ball.isDragging = false;
        dragging_ball = null;
        drag_start_time = null;
        drag_record = [];
        canvas.removeEventListener('mousemove', mousemove); 
    }
}

function keydown(e) {
    if (e.code == 'Space') {
        gravity_direction = -1;
    }
}

function keyup(e) {
    if (e.code == 'Space') {
        gravity_direction = 1;
    }
}

function resize() {
    canvas.width = viewport().width;
    canvas.height = viewport().height;
}
function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}

function draw() {
    window.requestAnimationFrame(draw);

    // do frame time / fps calc and show
    var current = Date.now();
    var dt = current - previous;
    previous = current;

    // clear screen
    context.fillStyle = 'black';
    context.fillRect(0, 0, canvas.width, canvas.height);

    for (let ball of balls) {
        ball.move(dt);    // move
    }
    for (let ball of balls) {
        ball.physics(dt); // run physic simulation
    }
    for (let ball of balls) {
        ball.draw();      // render
    }
    context.fillStyle = 'green';
    context.font = '12px Verdana';
    context.fillText((1000 / dt).toFixed(0) + 'fps', 5, 15);
}

// data about the ball
function Ball() {
    // radius
    this.r = map(Math.random(), 0, 1, 5, 25);

    // postion vector
    this.p = new Vector(map(Math.random(), 0, 1, this.r + 5, canvas.width  - this.r - 5), 
                        map(Math.random(), 0, 1, this.r + 5, canvas.height - this.r - 5));
    
    // mass
    // this.m = Math.PI * this.r * this.r; // mass = proportional to area = pi*r*r
    this.m = 4 / 3 * Math.PI * this.r * this.r * this.r; // mass = proportional to volume = 4/3 pi*r^3

    // velocity and acceleration for physics simulation
    this.v = new Vector(Math.random() * 25, Math.random() * 25);
    this.a = new Vector();

    this.isDragging = false;

    this.draw = function(){
        context.beginPath();
        context.fillStyle = this.getColor();
        context.arc(this.p.x, this.p.y, this.r, 0, Math.PI*2);
        context.fill();
    }

    this.getColor = function() {
        if (this.isDragging) 
            return 'blue';

        return (gravity_direction > 0) ? 'green' : 'red';

    }

    this.move = function(dt) { // includes fps correction
        if (this.isDragging) {
            drag_record.push({time:Date.now(), pos:this.p}); // only capture during frames;
            return;
        }

        this.p.add(this.v, dt / 1000);
        this.v.add(this.a, dt / 1000);
    }

    this.physics = function(dt) { // includes fps correction
        this.a.clear();

        if (this.isDragging) return;

        for (let ball of balls) {
            var distVector = Vector.sub(ball.p, this.p);
            var dist = distVector.getMag();
            if (dist) {
                // gravity force: F = G * (m1 + m2) / d^2
                dist = Math.max(dist, this.r + ball.r);
                var force = gravity_direction * gravity * (this.m + ball.m) / (dist * dist);
                distVector.setMag(force); // vector with correct direction and magnitude
                this.applyForce(distVector);

                // bounce
                if (dist <= this.r + ball.r) {
                    
                    // balls can get stuck deep in each other and keep flopping this 
                    // calculation around, so only do it if it makes the balls further apart next frame
                    var nextFramePos = Vector.add(this.p, Vector.mult(this.v, dt/1000));
                    var nextFrameDist = Vector.sub(ball.p, nextFramePos).getMag();
                    if (nextFrameDist < dist) {
                        solve_bounce2(this, ball, dt);
                    }
                }
            }

        }
        this.wallBounce();
    }

    this.collision = function (ball) {
        return Vector.sub(this.p, ball.p).getMag() <= this.r + ball.r;
    }

    this.wallBounce = function() {
        // bounce off walls
        var bounced = false;
        if (this.p.x <= this.r) {
            this.v.x = Math.abs(this.v.x);
            bounced = true;
        }
        else if (this.p.x + this.r >= canvas.width) {
            this.v.x = -1 * Math.abs(this.v.x);
            bounced = true;
        }
        if (this.p.y <= this.r) {
            this.v.y = Math.abs(this.v.y);
            bounced = true;
        }
        else if (this.p.y + this.r >= canvas.height) {
            this.v.y = -1 * Math.abs(this.v.y);
            bounced = true;
        }
        if (bounced) {
            this.v.mult(bounce_elasticity);
        }
    }

    this.applyForce = function(f) {
        // f = ma
        // a = f/m
        this.a.add(Vector.mult(f, 1/this.m));
    }
}

function Vector(x,y) {
    this.x = x || 0;
    this.y = y || 0;

    this.clear = function() {
        this.x = this.y = 0;
    }

    this.getMag = function() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    this.setMag = function(m) {
        var currentMag = this.getMag();
        if (currentMag) { // if this is 0,0, then magnitude is zero and we would divide by zero; just ignore
            var factor = m / currentMag;
            this.x *= factor;
            this.y *= factor;
        }    
    }

    this.add = function(v, scale) {
        scale = scale || 1;
        this.x += v.x * scale;
        this.y += v.y * scale;
    }
    this.sub = function(v, scale) {
        scale = scale || 1;
        this.x -= v.x * scale;
        this.y -= v.y * scale;
    }
    this.mult = function(s) {
        this.x *= s;
        this.y *= s;
    }
}

Vector.add = function(v2, v1) {
    return new Vector(v2.x + v1.x, v2.y + v1.y);
}

Vector.sub = function(v2, v1) {
    return new Vector(v2.x - v1.x, v2.y - v1.y);
}

Vector.mult = function(v, s) {
        return new Vector(v.x * s, v.y * s);
}

function map(input, in_min, in_max, out_min, out_max) {
  return (input - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function solve_bounce(ball1, ball2, dt) {
    var newVelX1 = (ball1.v.x * (ball1.m - ball2.m) + (2 * ball2.m * ball2.v.x)) / (ball1.m + ball2.m);
    var newVelY1 = (ball1.v.y * (ball1.m - ball2.m) + (2 * ball2.m * ball2.v.y)) / (ball1.m + ball2.m);
    var newVelX2 = (ball2.v.x * (ball2.m - ball1.m) + (2 * ball1.m * ball1.v.x)) / (ball1.m + ball2.m);
    var newVelY2 = (ball2.v.y * (ball2.m - ball1.m) + (2 * ball1.m * ball1.v.y)) / (ball1.m + ball2.m);

    if (!ball1.isDragging) ball1.v = new Vector(newVelX1, newVelY1);
    if (!ball2.isDragging) ball2.v = new Vector(newVelX2, newVelY2);

    if (!ball1.isDragging) ball1.p.add(ball1.v, dt / 1000);
    if (!ball2.isDragging) ball2.p.add(ball2.v, dt / 1000);
}

function solve_bounce2(ball1, ball2, dt) {
	var dx = ball1.p.x-ball2.p.x;
	var dy = ball1.p.y-ball2.p.y;
	var collision_angle = Math.atan2(dy, dx);
	var magnitude_1 = Math.sqrt(ball1.v.x*ball1.v.x+ball1.v.y*ball1.v.y);
	var magnitude_2 = Math.sqrt(ball2.v.x*ball2.v.x+ball2.v.y*ball2.v.y);
	var direction_1 = Math.atan2(ball1.v.y, ball1.v.x);
	var direction_2 = Math.atan2(ball2.v.y, ball2.v.x);
	var new_xspeed_1 = magnitude_1*Math.cos(direction_1-collision_angle);
	var new_yspeed_1 = magnitude_1*Math.sin(direction_1-collision_angle);
	var new_xspeed_2 = magnitude_2*Math.cos(direction_2-collision_angle);
	var new_yspeed_2 = magnitude_2*Math.sin(direction_2-collision_angle);
	var final_xspeed_1 = ((ball1.m-ball2.m)*new_xspeed_1+(ball2.m+ball2.m)*new_xspeed_2)/(ball1.m+ball2.m);
	var final_xspeed_2 = ((ball1.m+ball1.m)*new_xspeed_1+(ball2.m-ball1.m)*new_xspeed_2)/(ball1.m+ball2.m);
	var final_yspeed_1 = new_yspeed_1;
	var final_yspeed_2 = new_yspeed_2;
    if (!ball1.isDragging)  {
        ball1.v.x = Math.cos(collision_angle)*final_xspeed_1+Math.cos(collision_angle+Math.PI/2)*final_yspeed_1;
        ball1.v.y = Math.sin(collision_angle)*final_xspeed_1+Math.sin(collision_angle+Math.PI/2)*final_yspeed_1;
    }
    if (!ball2.isDragging)  {
        ball2.v.x = Math.cos(collision_angle)*final_xspeed_2+Math.cos(collision_angle+Math.PI/2)*final_yspeed_2;
        ball2.v.y = Math.sin(collision_angle)*final_xspeed_2+Math.sin(collision_angle+Math.PI/2)*final_yspeed_2;
    }

    if (!ball1.isDragging) ball1.p.add(ball1.v, dt / 1000);
    if (!ball2.isDragging) ball2.p.add(ball2.v, dt / 1000);

}